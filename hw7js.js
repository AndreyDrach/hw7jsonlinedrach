// Опишіть своїми словами як працює метод forEach.
// Метод перебирає кожен елемент масиву і виконує задану функцію.
// Як очистити масив?
// Є декілька способів, наприклад: arr.splice(0,arr.length) чи arr.length=0;
// Як можна перевірити, що та чи інша змінна є масивом?
// Array.isArray() - Повертає true, якщо значення є масивом, інакше повертає false.

const arr = ["hello", "world", 23, "23", null, true];

const filterBy = (arr, type) => {
  newArr = [];
  arr.forEach((elem) => {
    if (typeof elem !== type) {
      newArr = newArr.concat(elem);
    }
  });
  return newArr;
};

const allTypes = ["string", "number", "boolean"];
allTypes.forEach((type) => console.log(filterBy(arr, type)));

// 2-й способ-----------------------------------------------------------------------------------------------------------

function filterBy1(arr, type) {
  return arr.filter((elem) => typeof elem !== type);
}

allTypes.forEach((type) => console.log(filterBy1(arr, type)));
